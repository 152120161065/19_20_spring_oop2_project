﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Profile Form
    /// </summary>
    public partial class Form_Profile : Form
    {
        public Form_Profile()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Return Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReturn_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                Owner.Show();
                Hide();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnReturn.Name, message, DateTime.Now);
        }
        /// <summary>
        /// dgvPurchaseds_DataBindingComplete event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPurchaseds_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvPurchaseds.Columns["CustomerID"].Visible = false;
            dgvPurchaseds.Columns["Price"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("tr-TR");
            dgvPurchaseds.Columns["Price"].DefaultCellStyle.Format = "C2";
        }
        /// <summary>
        /// Form_Profile_FormClosing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Profile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
