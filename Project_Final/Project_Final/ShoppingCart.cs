﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// ShoppingCart Class
    /// </summary>
    public class ShoppingCart
    {
        /// <summary>
        /// CustomerId getter setter.
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// ItemsToPurchase getter setter.
        /// </summary>
        public BindingList<ItemToPurchase> ItemsToPurchase { get; set; }
        /// <summary>
        /// PaymentAmount getter setter.
        /// </summary>
        public decimal PaymentAmount { get; set; }
        /// <summary>
        /// PaymentType getter setter.
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// ShoppingCart Constructor
        /// </summary>
        /// <param name="customerId"></param>
        public ShoppingCart(int customerId)
        {
            CustomerId = customerId;
            ItemsToPurchase = new BindingList<ItemToPurchase>();
            PaymentAmount = 0;
        }
        /// <summary>
        /// void AddProduct function.
        /// </summary>
        /// <param name="item"></param>
        public void AddProduct(ItemToPurchase item)
        {
            ItemsToPurchase.Add(item);
            PaymentAmount += item.Product.Price;
        }
        /// <summary>
        /// void RemoveProduct function.
        /// </summary>
        /// <param name="item"></param>
        public void RemoveProduct(ItemToPurchase item)
        {
            ItemsToPurchase.Remove(item);
            PaymentAmount -= item.Product.Price;
        }
        /// <summary>
        /// void PrintProducts function.
        /// </summary>
        /// <param name="dgvItemsToPurchase"></param>
        public void PrintProducts(DataGridView dgvItemsToPurchase)
        {
            dgvItemsToPurchase.DataSource = ItemsToPurchase;
        }
        /// <summary>
        /// void PlaceOrder function.
        /// </summary>
        public void PlaceOrder()
        {
            int customerId = ActiveCustomer.GetInstance()._Customer.CustomerId;
            BindingList<ItemToPurchase> itemToPurchases = ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase;
            List<Purchased> ToPurchased = new List<Purchased>();
            foreach (var item in itemToPurchases)
            {
                ToPurchased.Add(new Purchased(item.Product.Name, item.Product.Price, customerId));
            }
            ActiveCustomer.GetInstance()._Customer.SaveCustomer(ToPurchased);
            SendInVoiceByMail();
            SendInVoiceBySms();
        }
        /// <summary>
        /// void SendInVoiceBySms function.
        /// </summary>
        private void SendInVoiceBySms()
        {
            MessageBox.Show("Your order has been received!", "SMS", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// void SendInVoiceByMail function.
        /// </summary>
        private void SendInVoiceByMail()
        {
            try
            {
                //Definition
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                //Setting for mail
                mail.From = new MailAddress("19.20.oop2.4@gmail.com");
                mail.To.Add(ActiveCustomer.GetInstance()._Customer.Email);
                mail.Subject = "Order Received";
                //Set Mail body
                mail.Body = "Your order has been received\n---------------------------------------\n" + ListOfPurchase();
                //Setting for smtp
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("19.20.oop2.4", "19_20_oop2_4");
                SmtpServer.EnableSsl = true;
                string userToken = "Success";
                //Send
                SmtpServer.SendAsync(mail, userToken);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// ListOfPurchase function.
        /// </summary>
        /// <returns>All items in ShoppingCart and total price</returns>
        private string ListOfPurchase()
        {
            string Result = string.Empty;
            foreach (var item in ItemsToPurchase)
            {
                Result += item.ToString();
            }
            return Result + "\n\nTotal Price: ₺" + PaymentAmount;
        }
    }
}
