﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// ShoppingCart Form.
    /// </summary>
    public partial class Form_ShoppingCart : Form
    {
        public Form_ShoppingCart()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Payment Page Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPaymentPage_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                Form paymentForm = new Form_Payment();
                paymentForm.Show(this);
                Hide();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnPaymentPage.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Remove Product Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemoveProduct_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                ActiveShoppingCart.GetInstance()._ShoppingCart.RemoveProduct(dgvShoppingCart.CurrentRow.DataBoundItem as ItemToPurchase);
                dgvShoppingCart.Refresh();
                if (dgvShoppingCart.Rows.Count.Equals(0))
                {
                    GoBack();
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnRemoveProduct.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Cancel Order Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelOrder_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase.Clear();
                ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentAmount = 0;
                GoBack();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnCancelOrder.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Return Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReturn_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                GoBack();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnReturn.Name, message, DateTime.Now);
        }
        /// <summary>
        /// GoBack function.
        /// </summary>
        private void GoBack()
        {
            foreach (var ctrl in Owner.Controls)
            {
                if (ctrl is LinkLabel)
                {
                    if (((LinkLabel)ctrl).Name.Equals("lnkShoppingCart"))
                    {
                        ((LinkLabel)ctrl).Text = "ShoppingCart(" + ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase.Count + ")";
                        break;
                    }
                }
            }
            Owner.Show();
            Hide();
        }
        /// <summary>
        /// Form_ShoppingCart_FormClosing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_ShoppingCart_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
