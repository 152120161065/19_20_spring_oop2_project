﻿namespace Project_Final
{
    partial class Form_ShoppingCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ShoppingCart));
            this.btnReturn = new System.Windows.Forms.Button();
            this.dgvShoppingCart = new System.Windows.Forms.DataGridView();
            this.btnCancelOrder = new System.Windows.Forms.Button();
            this.btnRemoveProduct = new System.Windows.Forms.Button();
            this.btnPaymentPage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShoppingCart)).BeginInit();
            this.SuspendLayout();
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnReturn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnReturn.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnReturn.Image = global::Project_Final.Properties.Resources.Log_Out_icon;
            this.btnReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReturn.Location = new System.Drawing.Point(781, 348);
            this.btnReturn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(136, 44);
            this.btnReturn.TabIndex = 8;
            this.btnReturn.Text = "Return";
            this.btnReturn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // dgvShoppingCart
            // 
            this.dgvShoppingCart.AllowUserToAddRows = false;
            this.dgvShoppingCart.AllowUserToDeleteRows = false;
            this.dgvShoppingCart.AllowUserToResizeColumns = false;
            this.dgvShoppingCart.AllowUserToResizeRows = false;
            this.dgvShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvShoppingCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShoppingCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dgvShoppingCart.BackgroundColor = System.Drawing.Color.White;
            this.dgvShoppingCart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvShoppingCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShoppingCart.Location = new System.Drawing.Point(15, 14);
            this.dgvShoppingCart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvShoppingCart.MultiSelect = false;
            this.dgvShoppingCart.Name = "dgvShoppingCart";
            this.dgvShoppingCart.ReadOnly = true;
            this.dgvShoppingCart.RowHeadersVisible = false;
            this.dgvShoppingCart.RowHeadersWidth = 51;
            this.dgvShoppingCart.RowTemplate.Height = 24;
            this.dgvShoppingCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShoppingCart.Size = new System.Drawing.Size(723, 516);
            this.dgvShoppingCart.TabIndex = 9;
            // 
            // btnCancelOrder
            // 
            this.btnCancelOrder.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancelOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnCancelOrder.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnCancelOrder.Image = global::Project_Final.Properties.Resources.Button_Close_icon;
            this.btnCancelOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelOrder.Location = new System.Drawing.Point(781, 281);
            this.btnCancelOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancelOrder.Name = "btnCancelOrder";
            this.btnCancelOrder.Size = new System.Drawing.Size(136, 44);
            this.btnCancelOrder.TabIndex = 7;
            this.btnCancelOrder.Text = "Cancel";
            this.btnCancelOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelOrder.UseVisualStyleBackColor = false;
            this.btnCancelOrder.Click += new System.EventHandler(this.btnCancelOrder_Click);
            // 
            // btnRemoveProduct
            // 
            this.btnRemoveProduct.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnRemoveProduct.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRemoveProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnRemoveProduct.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnRemoveProduct.Image = global::Project_Final.Properties.Resources.Button_Delete_icon;
            this.btnRemoveProduct.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRemoveProduct.Location = new System.Drawing.Point(781, 213);
            this.btnRemoveProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveProduct.Name = "btnRemoveProduct";
            this.btnRemoveProduct.Size = new System.Drawing.Size(136, 44);
            this.btnRemoveProduct.TabIndex = 6;
            this.btnRemoveProduct.Text = "Remove";
            this.btnRemoveProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRemoveProduct.UseVisualStyleBackColor = false;
            this.btnRemoveProduct.Click += new System.EventHandler(this.btnRemoveProduct_Click);
            // 
            // btnPaymentPage
            // 
            this.btnPaymentPage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnPaymentPage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPaymentPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPaymentPage.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnPaymentPage.Image = global::Project_Final.Properties.Resources.creditcard_icon;
            this.btnPaymentPage.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnPaymentPage.Location = new System.Drawing.Point(781, 146);
            this.btnPaymentPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPaymentPage.Name = "btnPaymentPage";
            this.btnPaymentPage.Size = new System.Drawing.Size(136, 44);
            this.btnPaymentPage.TabIndex = 5;
            this.btnPaymentPage.Text = "Payment";
            this.btnPaymentPage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPaymentPage.UseVisualStyleBackColor = false;
            this.btnPaymentPage.Click += new System.EventHandler(this.btnPaymentPage_Click);
            // 
            // Form_ShoppingCart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(927, 543);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnCancelOrder);
            this.Controls.Add(this.btnRemoveProduct);
            this.Controls.Add(this.btnPaymentPage);
            this.Controls.Add(this.dgvShoppingCart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form_ShoppingCart";
            this.Text = "Form_ShoppingCart";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_ShoppingCart_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShoppingCart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnCancelOrder;
        private System.Windows.Forms.Button btnRemoveProduct;
        private System.Windows.Forms.Button btnPaymentPage;
        private System.Windows.Forms.DataGridView dgvShoppingCart;
    }
}