﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Form_Menu Formu
    /// </summary>
    public partial class Form_Menu : Form
    {
        IProductDal[] productDals = new IProductDal[3]
        {
            BookDal.GetInstance(),
            MagazineDal.GetInstance(),
            MusicCDDal.GetInstance()
        };
        Form cartForm = new Form_ShoppingCart();
        Form profileForm = new Form_Profile();
        public Form_Menu()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Menu Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Menu_Load(object sender, EventArgs e)
        {
            rbBooks.Checked = true;
        }
        /// <summary>
        /// CategoryChanged event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CategoryChanged(object sender, EventArgs e)
        {
            dgvProducts.DataSource = null;
            if (rbBooks.Checked)
                productDals[0].GetAll(dgvProducts);
            if (rbMagazines.Checked)
                productDals[1].GetAll(dgvProducts);
            if (rbMusicCDs.Checked)
                productDals[2].GetAll(dgvProducts);
        }
        /// <summary>
        /// dgvProducts_DataBindingComplete event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvProducts_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgvProducts.Columns["Price"].DefaultCellStyle.FormatProvider = CultureInfo.GetCultureInfo("tr-TR");
            dgvProducts.Columns["Price"].DefaultCellStyle.Format = "C2";
        }
        /// <summary>
        /// Add button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                ShoppingCart _ShoppingCart = ActiveShoppingCart.GetInstance()._ShoppingCart;
                if (rbBooks.Checked)
                {
                    Book book = dgvProducts.CurrentRow.DataBoundItem as Book;
                    _ShoppingCart.AddProduct(new ItemToPurchase(book, 1));
                }
                if (rbMagazines.Checked)
                {
                    Magazine magazine = dgvProducts.CurrentRow.DataBoundItem as Magazine;
                    _ShoppingCart.AddProduct(new ItemToPurchase(magazine, 1));
                }
                if (rbMusicCDs.Checked)
                {
                    MusicCD musicCD = dgvProducts.CurrentRow.DataBoundItem as MusicCD;
                    _ShoppingCart.AddProduct(new ItemToPurchase(musicCD, 1));
                }
                lnkShoppingCart.Text = "ShoppingCart(" + _ShoppingCart.ItemsToPurchase.Count + ")";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnAdd.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Exit Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                lnkShoppingCart.Text = "ShoppingCart";
                Owner.Show();
                Hide();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnExit.Name, message, DateTime.Now);
        }
        /// <summary>
        /// lnkShoppingCart_LinkClicked event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkShoppingCart_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase.Count != 0)
            {
                GoCart();
            }
        }
        /// <summary>
        /// GoCart function.
        /// </summary>
        private void GoCart()
        {
            foreach (var ctrl in cartForm.Controls)
            {
                if (ctrl is DataGridView)
                {
                    ActiveShoppingCart.GetInstance()._ShoppingCart.PrintProducts((DataGridView)ctrl);
                    break;
                }
            }
            cartForm.Show(this);
            Hide();
        }
        /// <summary>
        /// Form_Menu_FormClosing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
        /// <summary>
        /// lnkProfile_LinkClicked event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lnkProfile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (Control ctrl in profileForm.Controls)
            {
                if (ctrl is Label)
                {
                    ctrl.Text = ActiveCustomer.GetInstance()._Customer.PrintCustomerDetails();
                }
                if (ctrl is DataGridView)
                {
                    ActiveCustomer.GetInstance()._Customer.PrintCustomerPurchases((DataGridView)ctrl);
                }
            }
            profileForm.Show(this);
            Hide();
        }
    }
}
