﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Class Customer
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// CustomerId getter setter.
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// Name getter setter.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Address getter setter.
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Email getter setter.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Username getter setter.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Password getter setter.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// PrintCustomerDetails function.
        /// </summary>
        /// <returns>CustomerId Name Email Username Adress</returns>
        public string PrintCustomerDetails()
        {
            return string.Format("Id: {0}\n\nName: {1}\n\nEmail: {2}\n\nUsername: {3}\n\nAddress: {4}", CustomerId, Name, Email, Username, Address);
        }
        /// <summary>
        /// PrintCustomerPurchases function.
        /// </summary>
        /// <param name="dgvPurchaseds"></param>
        public void PrintCustomerPurchases(DataGridView dgvPurchaseds)
        {
            PurchasedDal.GetInstance().GetAll((DataGridView)dgvPurchaseds);
        }
        /// <summary>
        /// SaveCustomer function.
        /// </summary>
        /// <param name="ToPurchased"></param>
        public void SaveCustomer(List<Purchased> ToPurchased)
        {
            PurchasedDal purchasedDal = PurchasedDal.GetInstance();
            purchasedDal.Add(ToPurchased);
        }
    }
}
