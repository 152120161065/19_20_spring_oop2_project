﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// PurchasedDal Class derived from IProductDal
    /// </summary>
    public class PurchasedDal : IProductDal
    {
        /// <summary>
        /// void getAll function.
        /// </summary>
        /// <param name="dgvPurchaseds"></param>
        public void GetAll(DataGridView dgvPurchaseds)
        {
            int id = ActiveCustomer.GetInstance()._Customer.CustomerId;
            using (ETradeContext context = new ETradeContext())
            {
                dgvPurchaseds.DataSource = context.Purchaseds.Where(p => p.CustomerId == id).ToList();
            }
        }
        /// <summary>
        /// void Add function.
        /// </summary>
        /// <param name="entities"></param>
        public void Add(List<Purchased> entities)
        {
            using (ETradeContext context = new ETradeContext())
            {
                context.Purchaseds.AddRange(entities);
                context.SaveChanges();
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static PurchasedDal instance;
        /// <summary>
        /// PurchasedDal Defaul Constructor.
        /// </summary>
        private PurchasedDal()
        { }
        /// <summary>
        /// PurchasedDal GetInstance function.
        /// </summary>
        /// <returns></returns>
        public static PurchasedDal GetInstance()
        {
            if (instance == null)
            {
                instance = new PurchasedDal();
            }
            return instance;
        }
    }
}
