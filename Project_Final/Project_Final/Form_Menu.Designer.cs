﻿namespace Project_Final
{
    partial class Form_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Menu));
            this.rbBooks = new System.Windows.Forms.RadioButton();
            this.rbMusicCDs = new System.Windows.Forms.RadioButton();
            this.rbMagazines = new System.Windows.Forms.RadioButton();
            this.lnkProfile = new System.Windows.Forms.LinkLabel();
            this.lnkShoppingCart = new System.Windows.Forms.LinkLabel();
            this.dgvProducts = new System.Windows.Forms.DataGridView();
            this.gbxCategories = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).BeginInit();
            this.gbxCategories.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbBooks
            // 
            this.rbBooks.AutoSize = true;
            this.rbBooks.Location = new System.Drawing.Point(5, 38);
            this.rbBooks.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbBooks.Name = "rbBooks";
            this.rbBooks.Size = new System.Drawing.Size(73, 21);
            this.rbBooks.TabIndex = 0;
            this.rbBooks.TabStop = true;
            this.rbBooks.Text = "Books";
            this.rbBooks.UseVisualStyleBackColor = true;
            this.rbBooks.CheckedChanged += new System.EventHandler(this.CategoryChanged);
            // 
            // rbMusicCDs
            // 
            this.rbMusicCDs.AutoSize = true;
            this.rbMusicCDs.Location = new System.Drawing.Point(5, 92);
            this.rbMusicCDs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbMusicCDs.Name = "rbMusicCDs";
            this.rbMusicCDs.Size = new System.Drawing.Size(99, 21);
            this.rbMusicCDs.TabIndex = 2;
            this.rbMusicCDs.TabStop = true;
            this.rbMusicCDs.Text = "MusicCDs";
            this.rbMusicCDs.UseVisualStyleBackColor = true;
            this.rbMusicCDs.CheckedChanged += new System.EventHandler(this.CategoryChanged);
            // 
            // rbMagazines
            // 
            this.rbMagazines.AutoSize = true;
            this.rbMagazines.Location = new System.Drawing.Point(5, 65);
            this.rbMagazines.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbMagazines.Name = "rbMagazines";
            this.rbMagazines.Size = new System.Drawing.Size(106, 21);
            this.rbMagazines.TabIndex = 1;
            this.rbMagazines.TabStop = true;
            this.rbMagazines.Text = "Magazines";
            this.rbMagazines.UseVisualStyleBackColor = true;
            this.rbMagazines.CheckedChanged += new System.EventHandler(this.CategoryChanged);
            // 
            // lnkProfile
            // 
            this.lnkProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkProfile.AutoSize = true;
            this.lnkProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lnkProfile.LinkColor = System.Drawing.Color.White;
            this.lnkProfile.Location = new System.Drawing.Point(733, 14);
            this.lnkProfile.Name = "lnkProfile";
            this.lnkProfile.Size = new System.Drawing.Size(55, 17);
            this.lnkProfile.TabIndex = 10;
            this.lnkProfile.TabStop = true;
            this.lnkProfile.Text = "Profile";
            this.lnkProfile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkProfile_LinkClicked);
            // 
            // lnkShoppingCart
            // 
            this.lnkShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkShoppingCart.AutoSize = true;
            this.lnkShoppingCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lnkShoppingCart.LinkColor = System.Drawing.Color.White;
            this.lnkShoppingCart.Location = new System.Drawing.Point(576, 14);
            this.lnkShoppingCart.Name = "lnkShoppingCart";
            this.lnkShoppingCart.Size = new System.Drawing.Size(127, 17);
            this.lnkShoppingCart.TabIndex = 9;
            this.lnkShoppingCart.TabStop = true;
            this.lnkShoppingCart.Text = "ShoppingCart(0)";
            this.lnkShoppingCart.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkShoppingCart_LinkClicked);
            // 
            // dgvProducts
            // 
            this.dgvProducts.AllowUserToAddRows = false;
            this.dgvProducts.AllowUserToDeleteRows = false;
            this.dgvProducts.AllowUserToResizeColumns = false;
            this.dgvProducts.AllowUserToResizeRows = false;
            this.dgvProducts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProducts.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dgvProducts.BackgroundColor = System.Drawing.Color.White;
            this.dgvProducts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducts.Location = new System.Drawing.Point(179, 50);
            this.dgvProducts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvProducts.MultiSelect = false;
            this.dgvProducts.Name = "dgvProducts";
            this.dgvProducts.ReadOnly = true;
            this.dgvProducts.RowHeadersVisible = false;
            this.dgvProducts.RowHeadersWidth = 51;
            this.dgvProducts.RowTemplate.Height = 24;
            this.dgvProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProducts.Size = new System.Drawing.Size(609, 386);
            this.dgvProducts.TabIndex = 11;
            this.dgvProducts.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvProducts_DataBindingComplete);
            // 
            // gbxCategories
            // 
            this.gbxCategories.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.gbxCategories.Controls.Add(this.rbBooks);
            this.gbxCategories.Controls.Add(this.rbMusicCDs);
            this.gbxCategories.Controls.Add(this.rbMagazines);
            this.gbxCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbxCategories.ForeColor = System.Drawing.SystemColors.Info;
            this.gbxCategories.Location = new System.Drawing.Point(19, 156);
            this.gbxCategories.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCategories.Name = "gbxCategories";
            this.gbxCategories.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxCategories.Size = new System.Drawing.Size(134, 145);
            this.gbxCategories.TabIndex = 6;
            this.gbxCategories.TabStop = false;
            this.gbxCategories.Text = "Categories";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExit.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExit.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnExit.Image = global::Project_Final.Properties.Resources.Log_Out_icon;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.Location = new System.Drawing.Point(19, 394);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(136, 44);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAdd.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAdd.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnAdd.Image = global::Project_Final.Properties.Resources.Shopping_Cart;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdd.Location = new System.Drawing.Point(19, 326);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(136, 44);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Form_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lnkProfile);
            this.Controls.Add(this.lnkShoppingCart);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvProducts);
            this.Controls.Add(this.gbxCategories);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form_Menu";
            this.Text = "MENU";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Menu_FormClosing);
            this.Load += new System.EventHandler(this.Form_Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).EndInit();
            this.gbxCategories.ResumeLayout(false);
            this.gbxCategories.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbBooks;
        private System.Windows.Forms.RadioButton rbMusicCDs;
        private System.Windows.Forms.RadioButton rbMagazines;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.LinkLabel lnkProfile;
        private System.Windows.Forms.LinkLabel lnkShoppingCart;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgvProducts;
        private System.Windows.Forms.GroupBox gbxCategories;
    }
}