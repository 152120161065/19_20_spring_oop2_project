﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// MusicCDDal Class derived from IProductDal
    /// </summary>
    public class MusicCDDal : IProductDal
    {
        /// <summary>
        /// void getAll function.
        /// </summary>
        /// <param name="dgvProducts"></param>
        public void GetAll(DataGridView dgvProducts)
        {
            using (ETradeContext context = new ETradeContext())
            {
                dgvProducts.DataSource = context.MusicCDs.ToList();
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static MusicCDDal instance;
        /// <summary>
        /// MusicCDDal Default Constructor.
        /// </summary>
        private MusicCDDal()
        { }
        /// <summary>
        /// MusicCDDal getInstance function.
        /// </summary>
        /// <returns></returns>
        public static MusicCDDal GetInstance()
        {
            if (instance == null)
            {
                instance = new MusicCDDal();
            }
            return instance;
        }
    }
}
