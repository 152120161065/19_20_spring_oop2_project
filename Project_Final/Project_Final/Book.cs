﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Book Class derived from Product 
    /// </summary>
    public class Book : Product
    {
        /// <summary>
        /// IsbnNumber getter setter.
        /// </summary>
        public string IsbnNumber { get; set; }
        /// <summary>
        /// Author getter setter.
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Publisher getter setter.
        /// </summary>
        public string Publisher { get; set; }
        /// <summary>
        /// Page getter setter.
        /// </summary>
        public int Page { get; set; }
    }
}
