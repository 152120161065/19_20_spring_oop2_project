﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Form_Login Formu
    /// </summary>
    public partial class Form_Login : Form
    {
        Form menu = new Form_Menu();
        CustomerDal customerDal = CustomerDal.GetInstance();
        public Form_Login()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Login Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                Customer customer;
                customer = customerDal.Validate(txtUsername.Text, txtPassword.Text);
                if (customer == null)
                    Login_Failed();
                else
                {
                    Login_Success();
                    ActiveCustomer.GetInstance()._Customer = customer;
                    ActiveShoppingCart.GetInstance()._ShoppingCart = new ShoppingCart(customer.CustomerId);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnLogin.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Login_Success function.
        /// </summary>
        private void Login_Success()
        {
            lblInfo.Text = string.Empty;
            txtPassword.Text = string.Empty;
            menu.Show(this);
            Hide();
        }
        /// <summary>
        /// Login_Failed function.
        /// </summary>
        private void Login_Failed()
        {
            lblInfo.Text = "Wrong username or password!";
        }
        /// <summary>
        /// Form_Login_FormClosing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
