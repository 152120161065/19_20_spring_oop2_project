﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Purchased Class
    /// </summary>
    public class Purchased
    {
        /// <summary>
        /// Id getter setter
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name getter setter
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Price getter setter
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// CustomerId getter setter
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// Purchased Constructor with Parameters
        /// </summary>
        /// <param name="name"></param>
        /// <param name="price"></param>
        /// <param name="customerId"></param>
        public Purchased(string name, decimal price, int customerId)
        {
            Name = name;
            Price = price;
            CustomerId = customerId;
        }
        /// <summary>
        /// Purchased Default Constructor
        /// </summary>
        public Purchased()
        { }
    }
}
