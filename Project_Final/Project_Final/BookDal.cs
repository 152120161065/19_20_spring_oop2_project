﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// BookDal Class derived from IPorudctDal
    /// </summary>
    public class BookDal : IProductDal
    {
        /// <summary>
        /// getAll function.
        /// </summary>
        /// <param name="dgvProducts"></param>
        public void GetAll(DataGridView dgvProducts)
        {
            using (ETradeContext context = new ETradeContext())
            {
                dgvProducts.DataSource = context.Books.ToList();
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static BookDal instance;
        /// <summary>
        /// BookDal Constructor
        /// </summary>
        private BookDal()
        { }
        /// <summary>
        /// BookDal GetInstance function.
        /// </summary>
        /// <returns>instance</returns>
        public static BookDal GetInstance()
        {
            if (instance == null)
            {
                instance = new BookDal();
            }
            return instance;
        }
    }
}
