﻿namespace Project_Final
{
    partial class Form_Profile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Profile));
            this.lblProfile = new System.Windows.Forms.Label();
            this.btnReturn = new System.Windows.Forms.Button();
            this.dgvPurchaseds = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseds)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProfile
            // 
            this.lblProfile.AutoSize = true;
            this.lblProfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblProfile.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblProfile.Location = new System.Drawing.Point(25, 42);
            this.lblProfile.MaximumSize = new System.Drawing.Size(300, 0);
            this.lblProfile.Name = "lblProfile";
            this.lblProfile.Size = new System.Drawing.Size(0, 17);
            this.lblProfile.TabIndex = 5;
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReturn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnReturn.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnReturn.Image = global::Project_Final.Properties.Resources.Log_Out_icon;
            this.btnReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReturn.Location = new System.Drawing.Point(28, 319);
            this.btnReturn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(136, 44);
            this.btnReturn.TabIndex = 3;
            this.btnReturn.Text = "Return";
            this.btnReturn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // dgvPurchaseds
            // 
            this.dgvPurchaseds.AllowUserToAddRows = false;
            this.dgvPurchaseds.AllowUserToDeleteRows = false;
            this.dgvPurchaseds.AllowUserToResizeColumns = false;
            this.dgvPurchaseds.AllowUserToResizeRows = false;
            this.dgvPurchaseds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPurchaseds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPurchaseds.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.dgvPurchaseds.BackgroundColor = System.Drawing.Color.White;
            this.dgvPurchaseds.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPurchaseds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPurchaseds.Location = new System.Drawing.Point(352, 14);
            this.dgvPurchaseds.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvPurchaseds.MultiSelect = false;
            this.dgvPurchaseds.Name = "dgvPurchaseds";
            this.dgvPurchaseds.ReadOnly = true;
            this.dgvPurchaseds.RowHeadersVisible = false;
            this.dgvPurchaseds.RowHeadersWidth = 51;
            this.dgvPurchaseds.RowTemplate.Height = 24;
            this.dgvPurchaseds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPurchaseds.Size = new System.Drawing.Size(399, 466);
            this.dgvPurchaseds.TabIndex = 4;
            this.dgvPurchaseds.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPurchaseds_DataBindingComplete);
            // 
            // Form_Profile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(765, 507);
            this.Controls.Add(this.lblProfile);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.dgvPurchaseds);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.Name = "Form_Profile";
            this.Text = "PROFILE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Profile_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPurchaseds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProfile;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.DataGridView dgvPurchaseds;
    }
}