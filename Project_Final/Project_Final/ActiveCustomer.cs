﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// ActiveCustomer Class
    /// </summary>
    public class ActiveCustomer
    {
        /// <summary>
        /// _Customer setter getter function.
        /// </summary>
        public Customer _Customer { get; set; }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static ActiveCustomer instance;
        /// <summary>
        /// ActiveCustomer Constructor
        /// </summary>
        private ActiveCustomer()
        { }
        /// <summary>
        /// ActiveCustomer getInstance
        /// </summary>
        /// <returns>instance</returns>
        public static ActiveCustomer GetInstance()
        {
            if (instance == null)
            {
                instance = new ActiveCustomer();
            }
            return instance;
        }
    }
}
