﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Program Class.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (ETradeContext context = new ETradeContext())
            {
                if (context.Customers.ToList().Count.Equals(0))
                {
                    context.Customers.AddRange(AddInitialCustomers());
                    context.Books.AddRange(AddInitialBooks());
                    context.Magazines.AddRange(AddInitialMagazines());
                    context.MusicCDs.AddRange(AddInitialMusicCDs());
                    context.SaveChanges();
                }
            }
            Application.Run(new Form_Login());
        }
        /// <summary>
        /// AddInitialCustomers function.
        /// </summary>
        /// <returns>initialCustomers</returns>
        private static List<Customer> AddInitialCustomers()
        {
            List<Customer> initialCustomers = new List<Customer>();
            initialCustomers.Add(new Customer { Name = "Bahadir", Address = "Aydin", Email = "152120161065@ogrenci.ogu.edu.tr", Username = "bahadir1", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Evren", Address = "Ankara", Email = "evren.rahimoglu@gmail.com", Username = "evren1", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Sertac", Address = "Eskisehir", Email = "srtc.demir@gmail.com", Username = "sertac1", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Fersu", Address = "Istanbul", Email = "fersuekinci@gmail.com", Username = "fersu1", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme1", Address = "Van", Email = "deneme1@gmail.com", Username = "deneme1", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme2", Address = "Hatay", Email = "deneme2@gmail.com", Username = "deneme2", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme3", Address = "Bursa", Email = "deneme3@gmail.com", Username = "deneme3", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme4", Address = "Kars", Email = "deneme4@gmail.com", Username = "deneme4", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme5", Address = "Antalya", Email = "deneme5@gmail.com", Username = "deneme5", Password = "123" });
            initialCustomers.Add(new Customer { Name = "Deneme6", Address = "Erzurum", Email = "deneme6@gmail.com", Username = "deneme6", Password = "123" });
            return initialCustomers;
        }
        /// <summary>
        /// AddInitialBooks function.
        /// </summary>
        /// <returns>initialBooks</returns>
        private static List<Book> AddInitialBooks()
        {
            List<Book> initialBooks = new List<Book>();
            initialBooks.Add(new Book { Name = "Melekler ve Seytanlar", Price = 20, Author = "Dan Brown", IsbnNumber = "001", Page = 400, Publisher = "Alfa" });
            initialBooks.Add(new Book { Name = "Attila", Price = 15, Author = "Peyami Safa", IsbnNumber = "002", Page = 200, Publisher = "Beta" });
            initialBooks.Add(new Book { Name = "Cengiz Han", Price = 10, Author = "Jacob Abott", IsbnNumber = "003", Page = 160, Publisher = "Theta" });
            initialBooks.Add(new Book { Name = "Kavgam", Price = 25, Author = "Adolf Hitler", IsbnNumber = "004", Page = 360, Publisher = "Moon" });
            initialBooks.Add(new Book { Name = "Dava", Price = 17, Author = "Franz Kafka", IsbnNumber = "005", Page = 260, Publisher = "Sun" });
            return initialBooks;
        }
        /// <summary>
        /// AddInitialMagazines function.
        /// </summary>
        /// <returns>initialMagazines</returns>
        private static List<Magazine> AddInitialMagazines()
        {
            List<Magazine> initialMagazines = new List<Magazine>();
            initialMagazines.Add(new Magazine { Name = "Penguen", Price = 15, Issue = "Caricature", Type = "Entertainment" });
            initialMagazines.Add(new Magazine { Name = "Atlas", Price = 30, Issue = "Turkey", Type = "Geography" });
            initialMagazines.Add(new Magazine { Name = "NtvTarih", Price = 20, Issue = "Ottoman Empire", Type = "History" });
            initialMagazines.Add(new Magazine { Name = "FourFourTwo", Price = 10, Issue = "Football", Type = "Sport" });
            initialMagazines.Add(new Magazine { Name = "Cinema", Price = 20, Issue = "Cinema", Type = "Art" });
            return initialMagazines;
        }
        /// <summary>
        /// AddInitialMusicCDs function.
        /// </summary>
        /// <returns>initialMusicCDs</returns>
        private static List<MusicCD> AddInitialMusicCDs()
        {
            List<MusicCD> initialMusicCDs = new List<MusicCD>();
            initialMusicCDs.Add(new MusicCD { Name = "Turkuler", Price = 35, Singer = "Neset Ertas", Type = "Folk" });
            initialMusicCDs.Add(new MusicCD { Name = "Sandik", Price = 20, Singer = "Hayko Cepkin", Type = "Rock" });
            initialMusicCDs.Add(new MusicCD { Name = "My Way", Price = 30, Singer = "Frank Sinatra", Type = "Blues" });
            initialMusicCDs.Add(new MusicCD { Name = "Mutlu Ol Yeter", Price = 40, Singer = "Muslum Gurses", Type = "Slow" });
            initialMusicCDs.Add(new MusicCD { Name = "The Dark Side", Price = 20, Singer = "Pink Floyd", Type = "Rock" });
            return initialMusicCDs;
        }
    }
}
