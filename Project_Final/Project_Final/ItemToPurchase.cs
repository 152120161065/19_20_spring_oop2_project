﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Class ItemToPurchase
    /// </summary>
    public class ItemToPurchase
    {
        /// <summary>
        /// Product getter setter.
        /// </summary>
        public Product Product { get; set; }
        /// <summary>
        /// Quantity getter setter.
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// ItemToPurchase constructor.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        public ItemToPurchase(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }
        /// <summary>
        /// ToString function.
        /// </summary>
        /// <returns>Product.Name, Product.Price</returns>
        public override string ToString()
        {
            return string.Format("{0} || ₺{1}\n", Product.Name, Product.Price);
        }
    }
}
