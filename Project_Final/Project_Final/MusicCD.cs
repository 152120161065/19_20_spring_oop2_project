﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// MusicCD Class derived from Product
    /// </summary>
    public class MusicCD : Product
    {
        /// <summary>
        /// Singer setter getter.
        /// </summary>
        public string Singer { get; set; }
        /// <summary>
        /// Type setter getter.
        /// </summary>
        public string Type { get; set; }
    }
}
