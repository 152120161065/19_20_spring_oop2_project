﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Payment Form
    /// </summary>
    public partial class Form_Payment : Form
    {
        public Form_Payment()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Form_Payment_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Payment_Load(object sender, EventArgs e)
        {
            lblTotalPrice.Text = "Total Price :\n  ₺" + ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentAmount;
            rbCredit.Checked = true;
        }
        /// <summary>
        /// PaymentType_Change event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PaymentType_Change(object sender, EventArgs e)
        {
            if (rbCash.Checked)
            {
                gbxCartDetails.Enabled = false;
                return;
            }
            gbxCartDetails.Enabled = true;
        }
        /// <summary>
        /// Place Order button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPlaceOrder_Click(object sender, EventArgs e)
        {
            string message;
            try
            {
                if (rbCredit.Checked)
                {
                    ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentType = "Credit Cart";
                    if (mtxtCreditCart.MaskFull && txtOwner.Text != string.Empty && mtxtCvc.MaskFull)
                    {
                        PlaceOrder();
                        message = "Success, " + ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentType;
                    }
                    else
                    {
                        MessageBox.Show("Check cart details!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        message = "Failed Cart Info";
                    }
                }
                else
                {
                    ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentType = "Cash";
                    PlaceOrder();
                    message = "Success, " + ActiveShoppingCart.GetInstance()._ShoppingCart.PaymentType;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnPlaceOrder.Name, message, DateTime.Now);
        }
        /// <summary>
        /// PlaceOrder function.
        /// </summary>
        private void PlaceOrder()
        {
            //TODO : To be implemented
            ActiveShoppingCart.GetInstance()._ShoppingCart.PlaceOrder();
            //Go to menu
            ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase.Clear();
            foreach (var ctrl in Owner.Owner.Controls)
            {
                if (ctrl is LinkLabel)
                {
                    Control tCtrl = ctrl as LinkLabel;
                    if (tCtrl.Name.Equals("lnkShoppingCart"))
                    {
                        tCtrl.Text = "ShoppingCart(" + ActiveShoppingCart.GetInstance()._ShoppingCart.ItemsToPurchase.Count + ")";
                        break;
                    }
                }
            }
            Owner.Owner.Show();
            Hide();
        }
        /// <summary>
        /// Return button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReturn_Click(object sender, EventArgs e)
        {
            string message = "Success";
            try
            {
                Owner.Show();
                Hide();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            LoggerDal.GetInstance().Add(btnReturn.Name, message, DateTime.Now);
        }
        /// <summary>
        /// Form_Payment_FormClosing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Payment_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Are You Sure?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
