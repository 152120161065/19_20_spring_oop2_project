﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// LoggerDal Class derived from ILoggerDal
    /// </summary>
    public class LoggerDal : ILoggerDal
    {
        /// <summary>
        /// void Add function.
        /// </summary>
        /// <param name="buttonName"></param>
        /// <param name="message"></param>
        /// <param name="time"></param>
        public void Add(string buttonName, string message, DateTime time)
        {
            using (ETradeContext context = new ETradeContext())
            {
                using (Log log = new Log(buttonName, message, ActiveCustomer.GetInstance()._Customer.CustomerId, time))
                {
                    context.Logs.Add(log);
                    context.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static LoggerDal instance;
        /// <summary>
        /// LoggerDal Default Constructor
        /// </summary>
        private LoggerDal()
        { }
        /// <summary>
        /// LoggerDal getInstance function.
        /// </summary>
        /// <returns>instance</returns>
        public static LoggerDal GetInstance()
        {
            if (instance == null)
            {
                instance = new LoggerDal();
            }
            return instance;
        }
    }
}
