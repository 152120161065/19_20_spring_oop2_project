﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Interface ICustomerDal
    /// </summary>
    interface ICustomerDal
    {
        /// <summary>
        /// Customer Validate function.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Customer Validate(string username, string password);
    }
}
