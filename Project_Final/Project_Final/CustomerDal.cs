﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// CustomerDal Class derived from ICustomerDal
    /// </summary>
    public class CustomerDal : ICustomerDal
    {
        /// <summary>
        /// Validate function
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Customer Validate(string username, string password)
        {
            using (ETradeContext context = new ETradeContext())
            {
                return context.Customers.FirstOrDefault(p => p.Username.Equals(username) && p.Password.Equals(password));
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static CustomerDal instance;
        /// <summary>
        /// CustomerDal Constructor.
        /// </summary>
        private CustomerDal()
        { }
        /// <summary>
        /// CustomerDal GetInstance function.
        /// </summary>
        /// <returns>instance</returns>
        public static CustomerDal GetInstance()
        {
            if (instance == null)
            {
                instance = new CustomerDal();
            }
            return instance;
        }
    }
}
