﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Magazine Class derived from Product
    /// </summary>
    public class Magazine : Product
    {
        /// <summary>
        /// Issue setter getter.
        /// </summary>
        public string Issue { get; set; }
        /// <summary>
        /// Type setter getter.
        /// </summary>
        public string Type { get; set; }
    }
}
