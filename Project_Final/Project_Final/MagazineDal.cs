﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// MagazineDal Class derived from IProductDal
    /// </summary>
    public class MagazineDal : IProductDal
    {
        /// <summary>
        /// void getAll function.
        /// </summary>
        /// <param name="dgvProducts"></param>
        public void GetAll(DataGridView dgvProducts)
        {
            using (ETradeContext context = new ETradeContext())
            {
                dgvProducts.DataSource = context.Magazines.ToList();
            }
        }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static MagazineDal instance;
        /// <summary>
        /// MagazineDal Default Constructor.
        /// </summary>
        private MagazineDal()
        { }
        /// <summary>
        /// MagazineDal getInstance function.
        /// </summary>
        /// <returns>instance</returns>
        public static MagazineDal GetInstance()
        {
            if (instance == null)
            {
                instance = new MagazineDal();
            }
            return instance;
        }
    }
}
