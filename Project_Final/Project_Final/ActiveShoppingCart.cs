﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// ActiveShoppingCart Class
    /// </summary>
    public class ActiveShoppingCart
    {
        /// <summary>
        /// _ShoppingCart getter setter function
        /// </summary>
        public ShoppingCart _ShoppingCart { get; set; }
        /// <summary>
        /// Singleton Design Pattern
        /// </summary>
        private static ActiveShoppingCart instance;
        /// <summary>
        /// ActiveShoppingCart Constructor
        /// </summary>
        private ActiveShoppingCart()
        { }
        /// <summary>
        /// ActiveShoppingCart GetInstance
        /// </summary>
        /// <returns>instance</returns>
        public static ActiveShoppingCart GetInstance()
        {
            if (instance == null)
            {
                instance = new ActiveShoppingCart();
            }
            return instance;
        }
    }
}
