﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Log Class derived from IDisposable
    /// </summary>
    public class Log : IDisposable
    {
        /// <summary>
        /// Id getter setter.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name getter setter.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Message getter setter.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// CustomerId getter setter.
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// Time getter setter.
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// disposedValue set default false.
        /// </summary>
        private bool disposedValue = false;
        /// <summary>
        /// Log Default Constructor.
        /// </summary>
        public Log()
        { }
        /// <summary>
        /// Log Constructor with Parameters.
        /// </summary>
        public Log(string name, string message, int customerId, DateTime time)
        {
            Name = name;
            Message = message;
            CustomerId = customerId;
            Time = time;
        }
        /// <summary>
        /// virtual void Dispose funtion.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;
            }
        }
        /// <summary>
        /// void Dispose function.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Log Destructor.
        /// </summary>
        ~Log()
        {
            Dispose(false);
        }
    }
}
