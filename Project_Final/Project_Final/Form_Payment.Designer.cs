﻿namespace Project_Final
{
    partial class Form_Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Payment));
            this.mtxtCreditCart = new System.Windows.Forms.MaskedTextBox();
            this.gbxCartDetails = new System.Windows.Forms.GroupBox();
            this.dtpExpire = new System.Windows.Forms.DateTimePicker();
            this.lblCvc = new System.Windows.Forms.Label();
            this.lblExpireDate = new System.Windows.Forms.Label();
            this.lblCartNumber = new System.Windows.Forms.Label();
            this.lblOwner = new System.Windows.Forms.Label();
            this.mtxtCvc = new System.Windows.Forms.MaskedTextBox();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.gbxTotalPrice = new System.Windows.Forms.GroupBox();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.rbCash = new System.Windows.Forms.RadioButton();
            this.rbCredit = new System.Windows.Forms.RadioButton();
            this.btnReturn = new System.Windows.Forms.Button();
            this.btnPlaceOrder = new System.Windows.Forms.Button();
            this.gbxCartDetails.SuspendLayout();
            this.gbxTotalPrice.SuspendLayout();
            this.SuspendLayout();
            // 
            // mtxtCreditCart
            // 
            this.mtxtCreditCart.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.mtxtCreditCart.Location = new System.Drawing.Point(144, 117);
            this.mtxtCreditCart.Mask = "(9999)-(9999)-(9999)-(9999)";
            this.mtxtCreditCart.Name = "mtxtCreditCart";
            this.mtxtCreditCart.Size = new System.Drawing.Size(213, 22);
            this.mtxtCreditCart.TabIndex = 1;
            // 
            // gbxCartDetails
            // 
            this.gbxCartDetails.Controls.Add(this.dtpExpire);
            this.gbxCartDetails.Controls.Add(this.lblCvc);
            this.gbxCartDetails.Controls.Add(this.lblExpireDate);
            this.gbxCartDetails.Controls.Add(this.lblCartNumber);
            this.gbxCartDetails.Controls.Add(this.lblOwner);
            this.gbxCartDetails.Controls.Add(this.mtxtCvc);
            this.gbxCartDetails.Controls.Add(this.txtOwner);
            this.gbxCartDetails.Controls.Add(this.mtxtCreditCart);
            this.gbxCartDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbxCartDetails.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxCartDetails.Location = new System.Drawing.Point(16, 86);
            this.gbxCartDetails.Name = "gbxCartDetails";
            this.gbxCartDetails.Size = new System.Drawing.Size(363, 301);
            this.gbxCartDetails.TabIndex = 8;
            this.gbxCartDetails.TabStop = false;
            this.gbxCartDetails.Text = "Cart Details";
            // 
            // dtpExpire
            // 
            this.dtpExpire.CustomFormat = "MM/yyyy";
            this.dtpExpire.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpire.Location = new System.Drawing.Point(144, 171);
            this.dtpExpire.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.dtpExpire.MinDate = new System.DateTime(2020, 1, 1, 0, 0, 0, 0);
            this.dtpExpire.Name = "dtpExpire";
            this.dtpExpire.ShowUpDown = true;
            this.dtpExpire.Size = new System.Drawing.Size(97, 22);
            this.dtpExpire.TabIndex = 2;
            // 
            // lblCvc
            // 
            this.lblCvc.AutoSize = true;
            this.lblCvc.Location = new System.Drawing.Point(247, 171);
            this.lblCvc.Name = "lblCvc";
            this.lblCvc.Size = new System.Drawing.Size(53, 17);
            this.lblCvc.TabIndex = 7;
            this.lblCvc.Text = "CVC : ";
            // 
            // lblExpireDate
            // 
            this.lblExpireDate.AutoSize = true;
            this.lblExpireDate.Location = new System.Drawing.Point(17, 172);
            this.lblExpireDate.Name = "lblExpireDate";
            this.lblExpireDate.Size = new System.Drawing.Size(102, 17);
            this.lblExpireDate.TabIndex = 6;
            this.lblExpireDate.Text = "Expire Date :";
            // 
            // lblCartNumber
            // 
            this.lblCartNumber.AutoSize = true;
            this.lblCartNumber.Location = new System.Drawing.Point(17, 117);
            this.lblCartNumber.Name = "lblCartNumber";
            this.lblCartNumber.Size = new System.Drawing.Size(109, 17);
            this.lblCartNumber.TabIndex = 5;
            this.lblCartNumber.Text = "Cart Number :";
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(17, 60);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(98, 17);
            this.lblOwner.TabIndex = 4;
            this.lblOwner.Text = "Card Owner:";
            // 
            // mtxtCvc
            // 
            this.mtxtCvc.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.mtxtCvc.Location = new System.Drawing.Point(306, 173);
            this.mtxtCvc.Mask = "(999)";
            this.mtxtCvc.Name = "mtxtCvc";
            this.mtxtCvc.Size = new System.Drawing.Size(46, 22);
            this.mtxtCvc.TabIndex = 3;
            // 
            // txtOwner
            // 
            this.txtOwner.Location = new System.Drawing.Point(144, 60);
            this.txtOwner.MaxLength = 30;
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(213, 22);
            this.txtOwner.TabIndex = 0;
            // 
            // gbxTotalPrice
            // 
            this.gbxTotalPrice.Controls.Add(this.lblTotalPrice);
            this.gbxTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbxTotalPrice.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gbxTotalPrice.Location = new System.Drawing.Point(464, 127);
            this.gbxTotalPrice.Name = "gbxTotalPrice";
            this.gbxTotalPrice.Size = new System.Drawing.Size(255, 139);
            this.gbxTotalPrice.TabIndex = 11;
            this.gbxTotalPrice.TabStop = false;
            this.gbxTotalPrice.Text = "Total Price";
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTotalPrice.Location = new System.Drawing.Point(32, 48);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(119, 20);
            this.lblTotalPrice.TabIndex = 0;
            this.lblTotalPrice.Text = "Total Price : ";
            // 
            // rbCash
            // 
            this.rbCash.AutoSize = true;
            this.rbCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbCash.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbCash.Location = new System.Drawing.Point(165, 35);
            this.rbCash.Name = "rbCash";
            this.rbCash.Size = new System.Drawing.Size(65, 21);
            this.rbCash.TabIndex = 7;
            this.rbCash.TabStop = true;
            this.rbCash.Text = "Cash";
            this.rbCash.UseVisualStyleBackColor = true;
            this.rbCash.CheckedChanged += new System.EventHandler(this.PaymentType_Change);
            // 
            // rbCredit
            // 
            this.rbCredit.AutoSize = true;
            this.rbCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.rbCredit.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.rbCredit.Location = new System.Drawing.Point(16, 35);
            this.rbCredit.Name = "rbCredit";
            this.rbCredit.Size = new System.Drawing.Size(115, 21);
            this.rbCredit.TabIndex = 6;
            this.rbCredit.TabStop = true;
            this.rbCredit.Text = "Credit/Debit";
            this.rbCredit.UseVisualStyleBackColor = true;
            this.rbCredit.CheckedChanged += new System.EventHandler(this.PaymentType_Change);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReturn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnReturn.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnReturn.Image = global::Project_Final.Properties.Resources.Log_Out_icon;
            this.btnReturn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReturn.Location = new System.Drawing.Point(578, 320);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(141, 44);
            this.btnReturn.TabIndex = 10;
            this.btnReturn.Text = "Return";
            this.btnReturn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnPlaceOrder
            // 
            this.btnPlaceOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPlaceOrder.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPlaceOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPlaceOrder.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnPlaceOrder.Image = global::Project_Final.Properties.Resources.ok_icon;
            this.btnPlaceOrder.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPlaceOrder.Location = new System.Drawing.Point(423, 320);
            this.btnPlaceOrder.Name = "btnPlaceOrder";
            this.btnPlaceOrder.Size = new System.Drawing.Size(141, 44);
            this.btnPlaceOrder.TabIndex = 9;
            this.btnPlaceOrder.Text = "Place Order";
            this.btnPlaceOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPlaceOrder.UseVisualStyleBackColor = false;
            this.btnPlaceOrder.Click += new System.EventHandler(this.btnPlaceOrder_Click);
            // 
            // Form_Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(740, 422);
            this.Controls.Add(this.gbxCartDetails);
            this.Controls.Add(this.gbxTotalPrice);
            this.Controls.Add(this.rbCash);
            this.Controls.Add(this.rbCredit);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnPlaceOrder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Payment";
            this.Text = "PAYMENT";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Payment_FormClosing);
            this.Load += new System.EventHandler(this.Form_Payment_Load);
            this.gbxCartDetails.ResumeLayout(false);
            this.gbxCartDetails.PerformLayout();
            this.gbxTotalPrice.ResumeLayout(false);
            this.gbxTotalPrice.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox mtxtCreditCart;
        private System.Windows.Forms.GroupBox gbxCartDetails;
        private System.Windows.Forms.DateTimePicker dtpExpire;
        private System.Windows.Forms.Label lblCvc;
        private System.Windows.Forms.Label lblExpireDate;
        private System.Windows.Forms.Label lblCartNumber;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.MaskedTextBox mtxtCvc;
        private System.Windows.Forms.TextBox txtOwner;
        private System.Windows.Forms.GroupBox gbxTotalPrice;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.RadioButton rbCash;
        private System.Windows.Forms.RadioButton rbCredit;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnPlaceOrder;
    }
}