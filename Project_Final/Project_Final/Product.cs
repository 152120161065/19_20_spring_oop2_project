﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Abstract Class Product
    /// </summary>
    public abstract class Product
    {
        /// <summary>
        /// Id getter setter.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name getter setter.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Price getter setter.
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// ToString override function.
        /// </summary>
        /// <returns>Name</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
