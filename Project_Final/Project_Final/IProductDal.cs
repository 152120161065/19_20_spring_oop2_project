﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Final
{
    /// <summary>
    /// Interface IProductDal
    /// </summary>
    interface IProductDal
    {
        /// <summary>
        /// void getAll function.
        /// </summary>
        /// <param name="dgvProducts"></param>
        void GetAll(DataGridView dgvProducts);
    }
}
