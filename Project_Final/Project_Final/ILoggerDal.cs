﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// Interface ILoggerDal
    /// </summary>
    interface ILoggerDal
    {
        /// <summary>
        /// void Add function.
        /// </summary>
        /// <param name="buttonName"></param>
        /// <param name="message"></param>
        /// <param name="time"></param>
        void Add(string buttonName, string message, DateTime time);
    }
}
