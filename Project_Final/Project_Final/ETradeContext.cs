﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Final
{
    /// <summary>
    /// ETradeContext class derived from DbContext
    /// </summary>
    public class ETradeContext : DbContext
    {
        /// <summary>
        /// Books getter setter.
        /// </summary>
        public DbSet<Book> Books { get; set; }
        /// <summary>
        /// Magazines getter setter.
        /// </summary>
        public DbSet<Magazine> Magazines { get; set; }
        /// <summary>
        /// MusicCDs getter setter.
        /// </summary>
        public DbSet<MusicCD> MusicCDs { get; set; }
        /// <summary>
        /// Customers getter setter.
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
        /// <summary>
        /// Purchaseds getter setter.
        /// </summary>
        public DbSet<Purchased> Purchaseds { get; set; }
        /// <summary>
        /// Logs getter setter.
        /// </summary>
        public DbSet<Log> Logs { get; set; }
    }
}
